
class rock_fw::pre { 

  # Get the order right
  Firewall { require => undef, }

  # Implicit allow ICMP, will tailor this later
  #firewall { "000 accept all icmp requests": proto  => "icmp", action => "accept", }

  # Default firewall rules 
  firewall { '000 accept all icmp': proto => 'icmp', action => 'accept', } ->
  firewall { '001 accept all to lo interface': proto => 'all', iniface => 'lo', action => 'accept', } ->
  firewall { '002 accept related established rules': proto => 'all', state => ['RELATED', 'ESTABLISHED'], action => 'accept', } 

}

